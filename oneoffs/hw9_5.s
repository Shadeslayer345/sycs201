.data
    space: .asciiz " "
    end:   .asciiz "\n"
.text
main:
    li      $s0, 152
    li      $s1, 16
    addi    $s0, $s0, 4
    sub     $s0, $s0, $s1
    addi    $t0, $zero, 1024
    lb      $t0, -8($s0)
    sw      $t0, -12($s0)
DONE:
    li      $v0, 1
    move    $a0, $s0
    syscall

    li      $v0, 4
    la      $a0, space
    syscall

    li      $v0, 1
    move    $a0, $s1
    syscall

    li      $v0, 4
    la      $a0, end
    syscall

    li      $v0 10
    syscall
