.data
    space: .asciiz " "
    end:   .asciiz "\n"
.text
main:
    addi $t3, $zero, 137    # t3 starts at 137
    srl $t3, $t3, 5
    sll $s2, $t3, 2
    addi $t8, $s2, -6       # t8 stats at s2 - 6 =
    add $t3, $s2, $t3
LOOP: 
    slt $t2, $t8, $t3       # t2 is always 1 until t8 is less than t3
    beq $t2, $zero, DONE
ELSE: 
    addi $s2, $s2, 8
    addi $t8, $t8, 2
    j LOOP
DONE:
    li $v0, 1
    move $a0, $s2
    syscall

    li $v0, 4
    la $a0, space
    syscall

    li $v0, 1
    move $a0, $t2
    syscall

    li $v0, 4
    la $a0, space
    syscall

    li $v0, 1
    move $a0, $t3
    syscall

    li $v0, 4
    la $a0, space
    syscall

    li $v0, 1
    move $a0, $t8
    syscall

    li $v0, 4
    la $a0, space
    syscall

    li $v0, 4
    la $a0, end
    syscall

    li $v0, 10
    syscall
