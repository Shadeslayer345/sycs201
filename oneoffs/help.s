.data
    space: .asciiz " "
    end:   .asciiz "\n"
    arr:   .space 20
.text
main:
    la $t3, arr   # Load initial array

    li $t0, 5000
    sw $t0, ($t3)
    addi $t3, $t3, 4

    li $t0, 4000
    sw $t0, ($t3)
    addi $t3, $t3, 4

    li $t0, 3000
    sw $t0, ($t3)
    addi $t3, $t3, 4

    li $t0, 2000
    sw $t0, ($t3)
    addi $t3, $t3, 4

    li $t0, 1000
    sw $t0, ($t3)

    # ARRAY 5000 4000 3000 2000 1000

    lb $t0, ($t3)

    li $v0, 1
    move $a0, $t0
    syscall

    li $v0, 10
    syscall
