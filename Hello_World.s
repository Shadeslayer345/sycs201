# PROGRAM: Hello, World!

.data                              # Data declaration section
outstring: .ascii "\nHello World!\n"

.text                              # Assembly Language instructions
main:                              # Start of code section
    li $v0, 4
    la $a0, outstring              # system call code for printing string = 4
    syscall

    li $v0, 10
    syscall
