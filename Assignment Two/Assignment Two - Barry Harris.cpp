#include <iostream>

int main() {
    int nums[5];
    int min = 0;
    int max = 32768;
    int A = 0, B = 0, C = 0, D = 0, E = 0, sum = 0;

    for (int i = 0; i < 5; ++i) {
        int temp = 0;
        std::cin >> temp;
        if (temp > min && temp < max) {
            nums[i] = temp;
        } else {
            main();
        }
    }

    A = nums[0];
    B = nums[1];
    C = nums[2];
    D = nums[3];
    E = nums[4];

    sum = (A + B) - (C - D * E);

    std::cout << sum << std::endl;

}
