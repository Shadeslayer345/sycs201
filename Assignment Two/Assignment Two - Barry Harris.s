# Programming Assignment 2

.data
header:     .asciiz "\nAssignment #2 Program!\n-----------------------\n"
prompt:     .asciiz "\nPlease enter an integer "
answer:     .asciiz "(A + B) - (C - D * E) = "
plus:       .asciiz " + "
minus:      .asciiz " - "
multiply:   .asciiz " * "
equal:      .asciiz " = "
oparen:     .asciiz "("
cparen:     .asciiz ")"
# Reserving space for 5 element array.
array:      .space 20

.text
main:

# Print Header
li $v0, 4                   # System call for outputting strings
la $a0, header              # Loading word into registar
syscall

# Load array for insertion of integers 
la $t3, array               # Array pointer for traversing pointer
la $s0, array               # Array address one for referenceing

# Loop to read & strore input integers
add $t0, $zero, $zero       # initialize counter to 0
li $t1, 20                  # initialize max value to 4 iterations
insert_loop: beq $t0, $t1, exit
    li $v0, 4               # System call for outputting strings
    la $a0, prompt          # Loading word into registar
    syscall
    li $v0, 5
    syscall
    move $t2, $v0
    blt $t2, $zero, insert_loop
    bgt $t2, 32768, insert_loop
    sw $t2, ($t3)           # Inserts integer into if it passes checks
    li $v0, 1
    addi $t3, $t3, 4        # Increments the array pointer by 4
    addi $t0, $t0, 4        # increment counter by 4
    j insert_loop
exit:

end_insert_loop:
    # Load variables from array
    lw $s1, 0($s0)              #  A = array[0]
    lw $s2, 4($s0)             #  B = array[1]
    lw $s3, 8($s0)             #  C = array[2]
    lw $s4, 12($s0)            #  D = array[3]
    lw $s5, 16($s0)            #  E = array[4]

    add $t1, $s1, $s2          # Temp1 = (A + B)

    mul $t2, $s4, $s5          # Temp2 = (D * E)

    sub $t3, $s3, $t2          # Temp3 = (C - (D * E))

    sub $t4, $t1, $t3          # Temp4 = (A + B) - (C - D * E)


    # Print answer formatting
    li $v0, 1                 # System call code for printing integer = 1
    move $a0, $s1             # Load A into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, plus              # Load + into a0
    syscall                   # Output system call code

    li $v0, 1                 # System call code for printing integer = 1
    move $a0, $s2             # Load B into a0
    syscall                   # Output system call code
    

    li $v0, 4                 # System call code for printing string = 4
    la $a0, minus             # Load - into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, oparen            # Load ( into a0
    syscall                   # Output system call code

    li $v0, 1                 # System call code for printing integer = 1
    move $a0, $s3             # Load C into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, minus             # Load - into a0
    syscall                   # Output system call code
    
    li $v0, 1                 ## System call code for printing integer = 1
    move $a0, $s4             # Load D into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, multiply          # Load * into a0
    syscall                   # Output system call code

    li $v0, 1                 # System call code for printing integer = 1
    move $a0, $s5             # Load E into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, cparen            # Load ) into a0
    syscall                   # Output system call code

    li $v0, 4                 # System call code for printing string = 4
    la $a0, equal             # Load = into a0
    syscall                   # Output system call code

    # Print Difference
    li $v0, 1                      # System call code for printing integer = 1
    move $a0, $t4                  # Move value from arithmetic operation into a0
    syscall                        # Output system call code

    # Exit Program
    li $v0, 10                     # System call code for exiting program
    syscall
