#include <iostream>

int main() {
    int nums[5];
    int min = 0;
    int max = 32768;
    int A = 0, B = 0, C = 0, D = 0, E = 0, sum = 0;

    for (int i = 0; i < 5; ++i) {
        int temp = 0;
        std::cin >> temp;
        if (temp > min && temp < max) {
            nums[i] = temp;
        } else {
            i--;
        }
    }

    for (int i = 0; i < 5; ++i) {
        std::cout << nums[i] << " is " << std::hex << nums[i] << " in hex" <<
                std::endl;
    }
    return 0;
}
