#include <iostream>

int main() {
    int final, temp1, temp2, temp3, A, B, C, D, E;
    A = 10;
    B = 5;
    C = 15;
    D = 2;
    E = 3;

    temp1 = A + B;
    temp2 = D * E;
    temp3 = C - temp2;

    final = temp1 - temp3;

    std::cout << final << std::endl;

    return 0;
}
