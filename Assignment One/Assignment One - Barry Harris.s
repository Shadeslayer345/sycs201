# Programming Assignment #1

.data                              # Data declaration section
outstring: .ascii "\n Assignment #1 Program!\n"
formatstring: .ascii "-----------------------\n"
answerstring: .ascii "(A + B) - (C - D * E) = "

.text                              # Assembly Language instructions
main:                              # Start of code section

# Print Header
li $v0, 4
la $a0, outstring              # System call code for printing string = 4
syscall

# Load variable values
li $s0, 10                     #  A = 10
li $s1, 5                      #  B = 5
li $s2, 15                     #  C = 15
li $s3, 2                      #  D = 2
li $s4, 3                      #  E = 3


# Arithmetic Operations
add $t0, $s0, $s1              # Temp0 = (A + B)

mul $t1, $s3, $s4              # Temp1 = (D * E)

sub $t2, $s2, $t1              # Temp2 = (C - (D * E))

sub $t3, $t0, $t2              # Temp3 = (A + B) - (C - D * E)

# Print Difference
li $v0, 1                      # System call code for printing integer = 1
move $a0, $t3                  # Move value from arithmetic operation into a0
syscall                        # Output system call code

# Exit Program
li $v0, 10                    # System call code for exiting program
syscall

    
